﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace CoolParking.CM
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello User!");
            //Vehicle myCar = new Vehicle("AA-1234-FF", VehicleType.Bus, 100);
            //Console.WriteLine(myCar.Id);
            //Console.WriteLine(myCar.VehicleType);
            //Console.WriteLine(myCar.Balance);

            Console.WriteLine("___tests___");
            //start of time service
            ParkingService parkS = new ParkingService();
            Console.WriteLine("Parking bal: {0}", parkS.GetBalance());
            var listV = parkS.GetVehicles();
            Console.WriteLine("list of veh (with 2 veh)");
            Console.WriteLine("capacity: {0}", parkS.GetCapacity());
            Console.WriteLine("free spaces: {0}", parkS.GetFreePlaces());
            foreach (var car in listV)
            {
                Console.Write("- {0} / ", car.Id);
                Console.Write("{0} / ", car.Balance);
                Console.WriteLine("{0}", car.VehicleType);
            }
            Vehicle myCar1 = new Vehicle("XX-0000-XX", VehicleType.Truck, 1000);
            //Vehicle myCar2 = new Vehicle("BB-7890-CC", VehicleType.PassengerCar, 300);
            parkS.AddVehicle(myCar1);
            Console.WriteLine("list of veh:");
            Console.WriteLine("capacity: {0}", parkS.GetCapacity());
            Console.WriteLine("free spaces: {0}", parkS.GetFreePlaces());
            //var listV2 = parkS.GetVehicles();
            foreach (var car in listV)
            {
                Console.Write("- {0} / ", car.Id);
                Console.Write("{0} / ", car.Balance);
                Console.WriteLine("{0}", car.VehicleType);
            }
            parkS.TopUpVehicle("XX-0000-XX", 111);
            Console.WriteLine("list of veh(third has balance 1111):");
            foreach (var car in listV)
            {
                Console.Write("- {0} / ", car.Id);
                Console.Write("{0} / ", car.Balance);
                Console.WriteLine("{0}", car.VehicleType);
            }
            parkS.AddVehicle(new Vehicle("BB-7890-CC", VehicleType.PassengerCar, 300));
            parkS.AddVehicle(new Vehicle("BB-7890-FF", VehicleType.Truck, 1000)); 
            parkS.RemoveVehicle("XX-0000-XX");
            Console.WriteLine("list of veh(as first one):");
            Console.WriteLine("capacity: {0}", parkS.GetCapacity());
            Console.WriteLine("free spaces: {0}", parkS.GetFreePlaces());
            foreach (var car in listV)
            {
                Console.Write("- {0} / ", car.Id);
                Console.Write("{0} / ", car.Balance);
                Console.WriteLine("{0}", car.VehicleType);
            }
            
            Console.ReadLine();
        }
    }
}
