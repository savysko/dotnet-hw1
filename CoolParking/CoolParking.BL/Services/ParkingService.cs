﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService: IParkingService
    {
        Parking parking = Parking.GetInstance();
        private readonly  int _capacity = Settings.capacity;

        public void AddVehicle(Vehicle vehicle)
        {
            var freeSpace = GetFreePlaces();
            if (parking.VehiclesList.Exists(x => x.Id == vehicle.Id))
            {
                throw new ArgumentException(String.Format("{0} - this vehicle is on the list", vehicle.Id), "vehicle.Id");
            }
            else if(freeSpace == 0)
            {
                throw new InvalidOperationException(String.Format("{0} - there is no space", freeSpace));
            }
            else
            {
                parking.VehiclesList.Add(vehicle);
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return _capacity;
        }

        public int GetFreePlaces()
        {
            return _capacity - parking.VehiclesList.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehList = new ReadOnlyCollection<Vehicle>(parking.VehiclesList);
            return vehList;
        }

        public string ReadFromLog()
        {
            throw new NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!parking.VehiclesList.Exists(x => x.Id == vehicleId))
            {
                throw new InvalidOperationException(String.Format("the car {0} not exists", vehicleId));
            }
            else
            {
                var veh = parking.VehiclesList.FirstOrDefault(a => a.Id == vehicleId);
                if (veh.Balance <= 0)
                {
                    throw new InvalidOperationException(String.Format("the ballance is {0}", veh.Balance));
                }

                else
                {
                    var remInd = parking.VehiclesList.FindIndex(a => a.Id == vehicleId);
                    parking.VehiclesList.RemoveAt(remInd);
                }
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var topUpVeh = parking.VehiclesList.FirstOrDefault(a => a.Id == vehicleId);
            if (topUpVeh != null)
            {
                if (sum > 0)
                {
                    topUpVeh.Balance += sum;
                }
                else
                {
                    throw new ArgumentException(String.Format("sum is less than 0"));
                }
            }
            else
            {
                throw new ArgumentException(String.Format("vehicle not exists"));
            }
        }
    }
}
