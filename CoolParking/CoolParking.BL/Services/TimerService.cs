﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    class TimerService : ITimerService
    {
        public double Interval { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            DateTime localDate = DateTime.Now;
            Console.WriteLine("{0}", localDate.ToString("h:mm:ss tt"));
        }

        public void Stop()
        {
            DateTime localDate = DateTime.Now;
            Console.WriteLine("{0}", localDate.ToString("h:mm:ss tt"));
        }
    }
}