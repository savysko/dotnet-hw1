﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _instance;
        
        public decimal Balance { get; set; }
        public List<Vehicle> VehiclesList { get; set; }

        private Parking() {}
        public static Parking GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Parking();
                _instance.Balance = Settings.balance;
                _instance.VehiclesList = new List<Vehicle> {new Vehicle("AA-1234-FF", VehicleType.Bus, 100), new Vehicle("BB-2222-BB", VehicleType.PassengerCar, 300)}; // list should be empty
            }
            return _instance;
            
        }
    }
}