﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}