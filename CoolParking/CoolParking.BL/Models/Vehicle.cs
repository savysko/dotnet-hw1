﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }

        public Vehicle() {}
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex rgx = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
            if (!rgx.IsMatch(id))
            {
                throw new ArgumentException(String.Format("{0} don't match the pattern", id), "id");
            }
            else
            {
                Id = id;
            }

            if (!Enum.IsDefined(typeof(VehicleType), vehicleType))
            {
                throw new ArgumentException(String.Format("{0} should not be null", vehicleType), "vehicle type");
            }
            else
            {
                VehicleType = vehicleType;
            }

            if (balance < 0)
            {
                throw new ArgumentException(String.Format("{0} balance couldn't be less them zero", balance), "balance");
            }
            else
            {
                Balance = balance;
            }
        }

    }
}