﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Settings
    {
        public static decimal balance = 0;
        public static int capacity = 10;
        public static int paymentPeriod = 5;
        public static int logPeriod = 60;
        public static Dictionary<string, double> tarifs = new Dictionary<string, double> 
        {
            { "PassengerCar", 2 },
            { "Truck", 5 },
            { "Bus", 3.5 },
            { "Motorcycle", 1 }
        };
        public static double penaltyRatio = 2.5;
    }
}