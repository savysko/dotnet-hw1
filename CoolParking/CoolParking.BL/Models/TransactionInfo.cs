﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    struct TransactionInfo
    {
        public string VehicleId { get; set; }
        public DateTime TransactionTime { get; set; }
        public decimal Sum { get; set; }

    }
}